//
//  VehicleListPresenter.swift
//  Carfax_iOS_Assignment
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import Foundation

protocol VehicleListPresenterInterface {
    func buildVehicleListViewModel(response: Object?)
    func sortVehiclesByPriceHighToLow(items: [VehicleListCellItem]) -> [VehicleListCellItem]
    func sortVehiclesByPriceLowToHigh(items: [VehicleListCellItem]) -> [VehicleListCellItem]
}

class VehicleListPresenter: VehicleListPresenterInterface {
    
    weak var viewController: VehicleListController?
    var items = [VehicleListCellItem]()
    
    func buildVehicleListViewModel(response: Object?) {
        var viewModels = [VehicleListCellItem]()
        if let vehicleModels = response?.listings {
            for model in vehicleModels {
                let cellItem = createVehicleCellItem(vehicleModel: model)
                viewModels.append(cellItem)
            }
            items = viewModels
            viewController?.displayVehiclesResults(items: viewModels)
        }
    }
    
    func createVehicleCellItem(vehicleModel: Vehicle) -> VehicleListCellItem {
        
        let vehicleListCellItem = VehicleListCellItem()
        if let vehiclePhoto = vehicleModel.images?.firstPhoto?.medium {
            vehicleListCellItem.vehicleImageURL = URL(string: vehiclePhoto)
        }
        vehicleListCellItem.yearMakeModelTrim = formatVehicleYearMakeModelTrim(year: vehicleModel.year ?? 0, make: vehicleModel.make ?? "", model: vehicleModel.model ?? "", trim: vehicleModel.trim ?? "")
        vehicleListCellItem.price = formatVehiclePrice(price: vehicleModel.monthlyPaymentEstimate?.price ?? 0)
        vehicleListCellItem.mileage = formatVehicleMileage(mileage: vehicleModel.mileage ?? 0)
        if let city = vehicleModel.dealer?.city, let state = vehicleModel.dealer?.state {
            vehicleListCellItem.location = city + ", " + state
        }
        vehicleListCellItem.dealerPhoneNumber = formatPhoneNumber(number: vehicleModel.dealer?.phone ?? "")
        return vehicleListCellItem
    }
}

extension VehicleListPresenter {
    
    func formatVehicleYearMakeModelTrim(year:Int, make:String, model:String, trim:String) -> String {
        var formattedVehicleYearMakeModelTrim = ""
        if year != 0 {
            formattedVehicleYearMakeModelTrim += String(year)
        }
        formattedVehicleYearMakeModelTrim += " " + make + " " + model
        if trim != "Unspecified" {
            formattedVehicleYearMakeModelTrim += " " + trim
        }
        return formattedVehicleYearMakeModelTrim
    }
    
    func formatVehiclePrice(price:Int) -> String {
        var formattedVehiclePrice = ""
        if price != 0 {
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            // localize to your grouping and decimal separator
            currencyFormatter.locale = Locale.current
            formattedVehiclePrice = currencyFormatter.string(from: NSNumber(value: price))!
            formattedVehiclePrice = String(formattedVehiclePrice.dropLast(3))
        }
        return formattedVehiclePrice
    }
    
    func formatVehicleMileage(mileage:Int) -> String {
        var formattedVehicleMileage = ""
        if mileage != 0 {
            let number = Double(mileage)
            let thousand = number / 1000
            let million = number / 1000000
            if million >= 1.0 {
                formattedVehicleMileage = "\(round(million*10)/10)m"
            }
            else if thousand >= 1.0 {
                formattedVehicleMileage = "\(round(thousand*10)/10)k"
            }
            formattedVehicleMileage += " Mi"
        }
        return formattedVehicleMileage
    }
    
    private func formatPhoneNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX)XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    func unformatVehiclePrice(price:String) -> Float {
        var priceStr = price.replacingOccurrences(of: ",", with: "")
        priceStr = priceStr.replacingOccurrences(of: "$", with: "")
        return Float(priceStr)!
    }
    
    func sortVehiclesByPriceHighToLow(items: [VehicleListCellItem]) -> [VehicleListCellItem] {
        return items.sorted(by: { unformatVehiclePrice(price: $0.price!) > unformatVehiclePrice(price: $1.price!) })
    }
    
    func sortVehiclesByPriceLowToHigh(items: [VehicleListCellItem]) -> [VehicleListCellItem] {
        return items.sorted(by: { unformatVehiclePrice(price: $0.price!) < unformatVehiclePrice(price: $1.price!) })
    }
}
