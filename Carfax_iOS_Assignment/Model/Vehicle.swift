//
//  Vehicle.swift
//  Carfax_iOS_Assignment
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import Foundation


struct Object: Codable {
    var listings: [Vehicle]
}
struct Vehicle: Codable {
    var year: Int?
    var make: String?
    var model: String?
    var trim: String?
    var mileage: Int?
    var monthlyPaymentEstimate: MonthlyPaymentEstimate?
    var images: Images?
    var dealer: Dealer?

}
struct MonthlyPaymentEstimate: Codable {
    var price: Int?
}

struct Dealer: Codable {
    var phone: String?
    var city: String?
    var state: String?
}

struct Images: Codable {
    var firstPhoto: FirstPhoto?
}

struct FirstPhoto: Codable {
    var medium: String?
}
