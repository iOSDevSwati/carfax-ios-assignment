//
//  VehicleCollectionViewCell.swift
//  Carfax_iOS_Assignment
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import UIKit


class VehicleCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    // MARK: -
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var noImageAvailableLabel: UILabel!
    @IBOutlet weak var yearMakeModelTrimLabel: UILabel!
    @IBOutlet weak var vehiclePriceLabel: UILabel!
    @IBOutlet weak var vehicleMileageLabel: UILabel!
    @IBOutlet weak var vehicleLocationLabel: UILabel!
    @IBOutlet weak var callDealerButton: UIButton!
    @IBOutlet weak var cellWidthConstraint : NSLayoutConstraint!
    
    private lazy var gradient: CAGradientLayer? = {
        return CAGradientLayer()
    }()
    
    override init(frame: CGRect) {
    super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellWidthConstraint.constant = UIScreen.main.bounds.width
        // Initialization code
        vehicleImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        vehicleImageView.contentMode = .scaleAspectFill
        vehicleImageView.clipsToBounds = true
    }
    
    func configure(with item: VehicleListCellItem) {
        layoutIfNeeded()
        self.vehicleImageView.backgroundColor = hexStringToUIColor(hex: "EAEAEA")
        if let vehicleImageUrl = item.vehicleImageURL {
            noImageAvailableLabel.isHidden = true
                vehicleImageView.loadImageWith(url: vehicleImageUrl)
                self.vehicleImageView.layer.sublayers = nil
        } else {
            self.vehicleImageView.image = nil
            if let gradient = gradient {
                applyGradient(gradient, in: self.vehicleImageView)
            }
            noImageAvailableLabel.isHidden = false
        }
        
        yearMakeModelTrimLabel.text = item.yearMakeModelTrim
        vehiclePriceLabel.text = item.price
        vehicleMileageLabel.text = item.mileage
        vehicleLocationLabel.text = item.location
        callDealerButton.titleLabel?.text = item.dealerPhoneNumber
    }
    
    @IBAction func callDealerPhoneButtonTapped(_ sender: UIButton) {
        var phoneNumber = sender.titleLabel?.text!.replacingOccurrences(of: "(", with: "")
        phoneNumber = phoneNumber?.replacingOccurrences(of: ")", with: "")
        phoneNumber = phoneNumber?.replacingOccurrences(of: "-", with: "")

        guard let number = URL(string: "tel://" + phoneNumber!) else { return }
        UIApplication.shared.open(number)
    }
}
// MARK: - Private Helper Method
// MARK: -

extension VehicleCollectionViewCell {
    /**
     * add gradient layer on view
     */
    private func applyGradient(_ gradient: CAGradientLayer, in view: UIImageView) {
        gradient.colors = [hexStringToUIColor(hex: "#D9D9D9"), hexStringToUIColor(hex: "#ACACAC")]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.25, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        view.layer.addSublayer(gradient)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIImageView {
    
    func loadImageWith(url: URL) {
        image = nil
        URLSession.shared.dataTask(with: url) { data, _,error in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {
                return
            }
            DispatchQueue.main.async {
                if let downloadImage = UIImage(data: data) {
                    self.image = downloadImage
                }
            }
        }.resume()
    }
}
