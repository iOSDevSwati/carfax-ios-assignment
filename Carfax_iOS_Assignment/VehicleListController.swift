//
//  VehicleListController.swift
//  Carfax_iOS_Assignment
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import Foundation
import UIKit

class VehicleListController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    
    private var interactor: VehiclesInteractorInterface?
    private var presenter: VehicleListPresenterInterface?
    let searchController = UISearchController(searchResultsController: nil)
    private var arrayOfAllVehicles : [VehicleListCellItem] = []
    var filteredVehicles: [VehicleListCellItem] = []
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout! {
        didSet {
            collectionLayout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width, height: 360)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collectionView.register(UINib (nibName: "VehicleCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyCell")
        setupCleanArchStack()
        reloadVehicleList()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Year/Make/Model"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func setupCleanArchStack() {
        let viewController = self
        let interactor = VehicleListInteractor()
        let presenter = VehicleListPresenter()
        
        viewController.interactor = interactor
        viewController.presenter = presenter
        interactor.presenter = presenter
        presenter.viewController = viewController
        
    }
    
    func displayVehiclesResults(items: [VehicleListCellItem]) {
        arrayOfAllVehicles = items
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func reloadVehicleList() {
        interactor?.getVehicleList()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering {
            return filteredVehicles.count
        }
        return arrayOfAllVehicles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)
        let item : VehicleListCellItem
        if isFiltering {
            item = filteredVehicles[indexPath.row]
        } else {
            item = arrayOfAllVehicles[indexPath.row]
        }
        // Configure the cell
        if let cell = cell as? VehicleCollectionViewCell {
            cell.configure(with: item)
        }
        return cell
    }
    /**
     Sorting Action Sheet
     -
     */
    @IBAction func showSortActionSheet(controller: UIViewController) {
        let alert = UIAlertController(title: "Sort By:", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Price Low To High", style: .default, handler: { (_) in
            let sortedArray = self.presenter?.sortVehiclesByPriceLowToHigh(items: self.arrayOfAllVehicles)
            self.displayVehiclesResults(items: sortedArray!)
            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Price High To Low", style: .default, handler: { (_) in
            let sortedArray = self.presenter?.sortVehiclesByPriceHighToLow(items: self.arrayOfAllVehicles)
            self.displayVehiclesResults(items: sortedArray!)
            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredVehicles = arrayOfAllVehicles.filter { (vehicleListCellItem: VehicleListCellItem) -> Bool in
            return vehicleListCellItem.yearMakeModelTrim!.lowercased().contains(searchText.lowercased())
        }
        
        collectionView.reloadData()
    }
}

extension VehicleListController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
