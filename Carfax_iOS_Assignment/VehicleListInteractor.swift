//
//  VehicleListInteractor.swift
//  Carfax_iOS_Assignment
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import Foundation

protocol VehiclesInteractorInterface {
func getVehicleList()
}

class VehicleListInteractor: VehiclesInteractorInterface {
    var presenter: VehicleListPresenterInterface?
    var vehiclesListData: [Vehicle]?
    
    /// Build API command and getting response for all used vehicles list.
    func getVehicleList() {
        guard let url = URL(string: "https://carfax-for-consumers.firebaseio.com/assignment.json") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(Object.self, from:
                    dataResponse) //Decode JSON Response Data
                self.presenter?.buildVehicleListViewModel(response: model)
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
}

