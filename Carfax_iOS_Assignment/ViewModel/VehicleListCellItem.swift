//
//  VehicleListCellItem.swift
//  Carfax_iOS_Assignment
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import UIKit

class VehicleListCellItem: NSObject {
    var vehicleImageURL: URL?
    var yearMakeModelTrim: String?
    var price: String?
    var mileage: String?
    var location: String?
    var dealerPhoneNumber: String?
}
