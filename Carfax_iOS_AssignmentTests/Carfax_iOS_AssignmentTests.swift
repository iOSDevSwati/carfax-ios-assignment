//
//  Carfax_iOS_AssignmentTests.swift
//  Carfax_iOS_AssignmentTests
//
//  Created by Chawla, Swati (Cognizant) on 10/26/19.
//  Copyright © 2019 Chawla, Swati. All rights reserved.
//

import XCTest
@testable import Carfax_iOS_Assignment

class Carfax_iOS_AssignmentTests: XCTestCase {

    let presenter = VehicleListPresenter()
    var vehicleListResponse = Object(listings: [])
    var viewModels = [VehicleListCellItem]()
    
    lazy var vehicleImage = FirstPhoto(medium: "https://carfax-img.vast.com/carfax/-9050308143659109979/1/344x258")
    lazy var vehicleImageInfo = Images(firstPhoto: vehicleImage)


    lazy var vehicleDealerInfo = Dealer(phone: "8332202334", city: "Buena Park", state: "CA")
    lazy var vehicleMonthlyPaymentEstimate = MonthlyPaymentEstimate(price: 11994)
    lazy var vehicle = Vehicle(year: 2014, make: "Acura", model: "ILX", trim: "Unspecified", mileage: 82303, monthlyPaymentEstimate: vehicleMonthlyPaymentEstimate, images: vehicleImageInfo, dealer: vehicleDealerInfo)

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testBuildViewModel_GeneratedItemCountMatchesResponse() {

        let vehicleListResponse = Object(listings: [vehicle, vehicle])
        let expectedUpcomingStays = 2
        presenter.buildVehicleListViewModel(response: vehicleListResponse)
        XCTAssertEqual(presenter.items.count, expectedUpcomingStays)
    }

    func testNoVehiclesBuildViewModel() {
        let expectedUpcomingStaysCount = 0
        presenter.buildVehicleListViewModel(response: vehicleListResponse)
        XCTAssertEqual(presenter.items.count, expectedUpcomingStaysCount)
    }

    func testInitialCellItems() {
        let vehicleListResponse = Object(listings: [vehicle])
        presenter.buildVehicleListViewModel(response: vehicleListResponse)
        for cell in presenter.items {
            XCTAssertNotNil(cell)
        }
    }

    func testVehicleListCellItem_With_Valid_Response() {
        let cellItem = presenter.createVehicleCellItem(vehicleModel: vehicle)
        XCTAssertNotNil(cellItem)

        let vehiclePhotoURL = cellItem.vehicleImageURL?.absoluteString
        XCTAssertNotNil(vehiclePhotoURL)
        XCTAssertEqual(vehiclePhotoURL, "https://carfax-img.vast.com/carfax/-9050308143659109979/1/344x258")

        let vehicleYearMakeModelTrim = cellItem.yearMakeModelTrim
        XCTAssertNotNil(vehicleYearMakeModelTrim)
        XCTAssertEqual(vehicleYearMakeModelTrim, "2014 Acura ILX")

        let vehiclePrice = cellItem.price
        XCTAssertNotNil(vehiclePrice)
        XCTAssertEqual(vehiclePrice, "$11,994")
        
        let vehicleMileage = cellItem.mileage
        XCTAssertNotNil(vehicleMileage)
        XCTAssertEqual(vehicleMileage, "82.3k Mi")
        
        let vehicleLocation = cellItem.location
        XCTAssertNotNil(vehicleLocation)
        XCTAssertEqual(vehicleLocation, "Buena Park, CA")
        
        let vehicleDealerPhoneNumber = cellItem.dealerPhoneNumber
        XCTAssertNotNil(vehicleDealerPhoneNumber)
        XCTAssertEqual(vehicleDealerPhoneNumber, "(833)220-2334")
    }
}
